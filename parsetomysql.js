var express = require('express');

// Path to our public directory

var pub = __dirname;

// setup middleware

var app = express();
//app.use(app.router);
app.use(express.static(pub));
//app.use(express.errorHandler());

// Optional since express defaults to CWD/views

app.set('views', __dirname);

// Set our default template engine to "jade"
// which prevents the need for extensions
// (although you can still mix and match)
app.set('view engine', 'jade');

function User(name, email) {
  this.name = name;
  this.email = email;
}



// Dummy users
var users = [
    new User('tj', 'tj@vision-media.ca')
  , new User('ciaran', 'ciaranj@gmail.com')
  , new User('aaron', 'aaron.heckmann+github@gmail.com')
];
var bee = 54321;
app.get('/', function(req, res){
  res.render('index', { bee : bee});
});

app.listen(8080);
console.log('Express app started on port %d', 8080);