﻿using System.Linq;
using System.Xml.Linq;
using System.Windows.Forms;

namespace DBIS
{
    class XMLWriter
    {
        static XDocument doc;

        public XMLWriter()
        {
            
        }
        public static void Write()
        {
            if (MessageBox.Show("Данные будут записаны в xml файл. Продолжить?", "Внимание!", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                doc = new XDocument();
                doc.Add(new XElement("root"));
                foreach (object[] item in DBW.GetItemsLikeObjectArray())
                {
                    doc.Element("root").Add(new XElement("item", item.Select(x => new XElement("si", x))));
                }
                doc.Save("bridges.xml");
            }
        }
    }
}
