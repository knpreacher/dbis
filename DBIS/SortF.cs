﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBIS
{
    public partial class SortF : Form
    {
        public SortF()
        {
            InitializeComponent();
        }
        List<string> dditemsList = new List<string>() { "Код", "Название", "Длина", "Ширина", "Год постройки", "Стоимость постройки", "Открыт для пешеходов" };
        private void SortF_Load(object sender, EventArgs e)
        {
            comboBox1.DataSource = dditemsList;
            comboBox2.DataSource = uditems;
            //comboBox4.DataSource = uditems;

        }

        private void btnSort_Click(object sender, EventArgs e)
        {
            if (comboBox3.SelectedIndex >= tmp)
                p3 = comboBox3.SelectedIndex + 1;
            else p3 = comboBox3.SelectedIndex;
            //MessageBox.Show(string.Format("{0} {1} {2} {3}", comboBox1.SelectedIndex, comboBox2.SelectedIndex, p3, comboBox4.SelectedIndex));
            SortMany sm = new SortMany(comboBox1.SelectedIndex, p3, comboBox2.SelectedIndex,comboBox4.SelectedIndex);
            int lastSortedColumn = comboBox1.SelectedIndex;
            MainF.lsc = lastSortedColumn;
            int lastSortType = comboBox2.SelectedIndex;
            //SortMany sm = new SortMany(6, 0, 0);
            sm.Sort();
            sm.Fill(MainF.sdgv);
        }
        int p1, p2, p3, p4,tmp;
        List<string> uditems = new List<string>() { "По возрастанию", "По убыванию" };
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            p1 = comboBox1.SelectedIndex;
            List<string> sitems = new List<string>();
            //MessageBox.Show(comboBox1.SelectedIndex.ToString());
            for (int i = 0; i < dditemsList.Count; i++)
            {

                if (i != comboBox1.SelectedIndex)
                {
                    sitems.Add(dditemsList[i]);
                }
                else
                {
                    tmp = i;
                }
            }
            comboBox3.DataSource = sitems;
        }
    }
}
