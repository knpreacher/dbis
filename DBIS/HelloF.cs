﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBIS
{
    public partial class HelloF : Form
    {
        public bool isf { get; set; } = true;
        public HelloF()
        {
            InitializeComponent();
        }
        public HelloF(bool isf)
        {
            this.isf = isf;
            InitializeComponent();
        }

        private void HelloF_Load(object sender, EventArgs e)
        {
            pictureBox1.Visible = isf;
            progressBar1.Visible = isf;
            webBrowser1.Visible = !isf;
            Text = isf ? "" : "Помощь";
            ControlBox = !isf;
        }

        private void HelloF_Shown(object sender, EventArgs e)
        {
            if (isf)
            {
                Timer t = new Timer();
                t.Interval = 500;
                t.Tick += new EventHandler((object s, EventArgs ea) =>
                {
                    progressBar1.Value += 20;
                    if (progressBar1.Value == 100)
                    {
                        t.Stop();
                        Close();
                    }
                    //Sleep(1000);

                });
                t.Start();
                return;
            }
            webBrowser1.Url = new Uri("file:///E:/VSP/DBIS/html/index.html");
        }

        private void pictureBox1_LoadCompleted(object sender, AsyncCompletedEventArgs e)
        {
        }

        private void HelloF_ControlAdded(object sender, ControlEventArgs e)
        {

        }

        private void HelloF_Click(object sender, EventArgs e)
        {
            MessageBox.Show("eeee");
            webBrowser1.Url = new Uri("file:///E:/VSP/DBIS/html/index.html");
        }
    }
}
