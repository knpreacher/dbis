﻿using DBIS.TestDataSetTableAdapters;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBIS
{
    class DBW
    {
        DataGridView dgv;
        public static TestDataSet dataSet;
        public static genTableTableAdapter adapter;
        static TableAdapterManager manager;

        public DBW(DataGridView dgv)
        {
            this.dgv = dgv;
            adapter = new genTableTableAdapter();
            manager = new TableAdapterManager();
        }
        public static void FillDGV(List<object[]> list)
        {
            MainF.sdgv.RowCount = list.Count;
            for (int i = 0; i < list.Count; i++)
            {
                for (int j = 0; j < 7; j++)
                {
                    MainF.sdgv.Rows[i].Cells[j].Value = list[i][j];
                }
            }
        }
        public DBW()
        {

        }
        public struct Bridge
        {
            public int id;
            public string n;
            public int l;
            public int w;
            public int y;
            public int p;
            public bool o;

            
        }
        public static List<object[]> GetItemsLikeObjectArray()
        {
            List<object[]> list = new List<object[]>();
            foreach (DataRow item in adapter.GetData().Rows)
            {
                list.Add(item.ItemArray);
            }
            return list;
        }
        public List<Bridge> GetIntems()
        {
            List<Bridge> bridges = new List<Bridge>();
            foreach (DataRow item in adapter.GetData().Rows)
            {
                Bridge br = new Bridge();
                br.id = (int)item.ItemArray[0];
                br.n = (string)item.ItemArray[1];
                br.l = (int)item.ItemArray[2];
                br.w = (int)item.ItemArray[3];
                br.y = (int)item.ItemArray[4];
                br.p = (int)item.ItemArray[5];
                br.o = (bool)item.ItemArray[6];
                bridges.Add(br);
            }
            
            return bridges;
        }
        Task op; 
        public void Show()
        {
            op = new Task(()=> 
            {
                dataSet = new TestDataSet();
                SSS md = new SSS(SSSM);
                dgv.Invoke(md);
            });
            op.Start();
            
            
        }

        delegate void SSS();

        public static void Insert(string n, int l, int w, int y, int p, bool o)
        {

            TestDataSet.genTableRow row =
            dataSet.genTable.NewgenTableRow();
            row.Название = n;
            row.Длина = l;
            row.Ширина = w;
            row.Год_постройки = y;
            row.Стоимость_постройки = p;
            row.Открыт_для_пешеходов = o;
            dataSet.genTable.Rows.Add(row);
            adapter.Update(dataSet.genTable);
            manager.UpdateAll(dataSet);
            /*
            adapter.Insert(n,l,w,y,p,o);
            adapter.Adapter.Fill(dataSet);
            */
        }
        public static void Update(int id, string n, int l, int w, int y, int p, bool o)
        {
            adapter.UpdateQuery(n,l,w,y,p,o,id);
        }
        void SSSM()
        {
            try
            {
                //dgv.ColumnCount = 7;
                adapter = new genTableTableAdapter();
                adapter.GetData();
                adapter.Fill(dataSet.genTable);
                DataRowCollection rows = dataSet.genTable.Rows;
                int i = 0;
                dgv.RowCount = 1;
                foreach (DataRow item in rows)
                {
                    for (int j = 0; j < 7; j++)
                    {
                        dgv.Rows[i].Cells[j].Value = item.ItemArray[j];
                    }
                    i++;
                    dgv.RowCount++;
                }
                dgv.RowCount--;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        public void Delete(int code)
        {
            adapter.DeleteQuery(code);
            adapter.Adapter.Fill(dataSet);
        }
        public void Save()
        {
            
            if(MessageBox.Show("Изменения будут сохранены в базу. Продолжить?", "Внимание", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            try
            {
                File.Copy("E:\\VSP\\DBIS\\DBIS\\bin\\Debug\\Test.mdb", "E:\\VSP\\DBIS\\DBIS\\Test.mdb", true);
                MessageBox.Show("Сохранение выполнено успешно","Сохранено",MessageBoxButtons.OK,MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                //throw;
            }
            
        }
        public void RandGen()
        {
            Random r;
            int l;
            int w;
            int y;
            int p;
            bool o;
            for (int i = 0; i < 60; i++)
            {
                r = new Random();
                l = r.Next(1000,2500);
                r = new Random();
                w = r.Next(50,90);
                r = new Random();
                y = r.Next(2017,2060);
                r = new Random();
                p = l * w * r.Next(1,3)/100;
                r = new Random();
                o = r.Next(2) == 1 ? true : false;
                adapter.Insert("Мост Султана ", l, w, y, p, o);
            }
        }
        public void Stat()
        {
            MessageBox.Show(string.Format("Всего мостов {0}",dgv.RowCount),DateTime.Now.ToString());
        }
    }
}
