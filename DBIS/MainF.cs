﻿using DBIS.TestDataSetTableAdapters;
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace DBIS
{
    public partial class MainF : Form
    {
        DBW dbw;

        public MainF()
        {
            InitializeComponent();
            dbw = new DBW(dgv);
        }

        int lastSortedColumn = -1;
        int lastSortType = -1;

        TestDataSet dataset;
        genTableTableAdapter adapter;

        private void открытьОбновитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            dbw.Show();
        }

        private void добавитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //adapter.Insert("asd", 123, 123, 321, 324, false);
            ItemF itF = new ItemF();
            itF.ShowDialog();
        }

        public static DataGridView sdgv;

        private void сохранитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            dbw.Save();
        }

        private void dgv_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void dgv_Click(object sender, EventArgs e)
        {

        }

        private void dgv_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            DataGridViewCellCollection col = dgv.Rows[e.RowIndex].Cells;
            ItemF itF = new ItemF((int)col[0].Value,
                (string)col[1].Value,
                (int)col[2].Value,
                (int)col[3].Value,
                (int)col[4].Value,
                (int)col[5].Value,
                (bool)col[6].Value);
            
            itF.ShowDialog(this);
            //itF.Location = new Point(this.Location.X + 30, this.Location.Y + 30);

        }

        private void dgv_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.Handled)
            {
                MessageBox.Show("LOL");
            } 
        }

        private void dgv_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control)
            {
                if (e.KeyCode == Keys.R)
                    dbw.Show();
                else if (e.KeyCode == Keys.S)
                    dbw.Save();
                else if (e.KeyCode == Keys.Q)
                    dbw.RandGen();
                else if (e.KeyCode == Keys.B)
                    dbw.Stat();
                else if (e.KeyCode == Keys.F)
                    new SearchF(lastSortedColumn, lastSortType).Show();
                else if (e.KeyCode == Keys.M)
                    MYSQL();
                else if (e.KeyCode == Keys.X)
                    XMLWriter.Write();
            }
        }

        private void сортировкаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //сортировкаToolStripMenuItem.
            //MessageBox.Show("Shown");
            SortF sf = new SortF();
            sf.Show();
            
        }

        static string[] dditems = { "Код", "Название", "Длина", "Ширина", "Год постройки", "Стоимость постройки","Открыт для пешеходов" };

        public static int lsc = -1;

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            
            
        }
        
        private void MainF_Load(object sender, EventArgs e)
        {
            sdgv = dgv;
            List<string> dditemsList = new List<string>() { "Код", "Название", "Длина", "Ширина", "Год постройки", "Стоимость постройки", "Открыт для пешеходов" }; 
            
        }

        private void MainF_Click(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        void sortClick(object sender2, ToolStripItemClickedEventArgs e2)
        {

        }

        private void поискToolStripMenuItem_Click(object sender, EventArgs e)
        {
            lastSortedColumn = lsc;
            SearchF sf = new SearchF(lastSortedColumn,lastSortType);
            sf.Show();
        }
        private void MYSQL()
        {
            string CommandText = "SELECT * FROM bris";
            //string Connect = "Database=stambul;Data Source=174.138.66.202;User Id=knp;Password=looklook;Port=3306";
            string Connect = "database=stambul;host=174.138.66.202;user=knp;Password=looklook;";

            MySqlConnection myConnection = new MySqlConnection(Connect);
            MySqlCommand myCommand = new MySqlCommand(CommandText, myConnection);
            try
            {
                myConnection.Open();
                MessageBox.Show("eeeeeeeeeee");
                myConnection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            
        }

        private void помощьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void оПрограммеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new HelloF(false).Show();
        }
    }
}
