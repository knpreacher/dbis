﻿using System;
using System.Windows.Forms;

namespace DBIS
{
    public partial class ItemF : Form
    {
        int state = 0;
        int id;
        public ItemF()
        {
            InitializeComponent();
            state = 1;
        }
        public ItemF(int id,string n, int l, int w, int y, int p, bool o)
        {
            InitializeComponent();
            tbN.Text = n;
            tbL.Text = l.ToString();
            tbW.Text = w.ToString();
            tbY.Text = y.ToString();
            tbP.Text = p.ToString();
            cbO.Checked = o;
            this.id = id;
            state = 2;
        }

        private void ItemF_Load(object sender, EventArgs e)
        {
            if (state == 1)
            {
                btnSave.Location = new System.Drawing.Point(79,165);
                btnDel.Visible = false;
            }
            else
            {
                btnSave.Location = new System.Drawing.Point(162, 165);
                btnDel.Visible = true;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            switch (state)
            {
                case 0:
                    break;
                case 1:
                    try
                    {
                        DBW.Insert(tbN.Text,
                int.Parse(tbL.Text),
                int.Parse(tbW.Text),
                int.Parse(tbY.Text),
                int.Parse(tbP.Text),
                cbO.Checked);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);    
                        //throw;
                    }
                    break;
                case 2:
                    //Edit
                    DBW.Update(id,
                        tbN.Text,
                int.Parse(tbL.Text),
                int.Parse(tbW.Text),
                int.Parse(tbY.Text),
                int.Parse(tbP.Text),
                cbO.Checked);
                    break;
            }
            Close();
            
        }
    }
}
