﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBIS
{
    class Search
    {
        bool[] rb;
        int selectedIndex;
        public Search(int selectedIndex)
        {
            rb = new bool[7];
            this.selectedIndex = selectedIndex;
        }
        public void SetRBCheckedArray(
            bool rb0,
            bool rb1,
            bool rb2,
            bool rb3,
            bool rb4,
            bool rb5,
            bool rb6)
        {
            rb[0] = rb0;
            rb[1] = rb1;
            rb[2] = rb2;
            rb[3] = rb3;
            rb[4] = rb4;
            rb[5] = rb5;
            rb[6] = rb6;
        }
        int columnIndex = -1;
        List<object[]> list;
        public bool conts;
        public bool CheckSortedState()
        {
            list = DBW.GetItemsLikeObjectArray();
            for (int i = 0; i < rb.Length; i++)
            {
                if (rb[i])
                {
                    columnIndex = i;
                    if (columnIndex==selectedIndex)
                    {
                        conts = true;
                        MessageBox.Show(columnIndex.ToString() + " колонка отсортирована. Будет выполнен д поиск");

                        return true;
                    }
                    return false;
                }
               
            }
            return true;
        }
        List<List<object[]>> listPrev;
        public void Clearprev()
        {
            listPrev = new List<List<object[]>>();
            listPrev.Add(list);
        }
        int nquery = 0;
        public List<object[]> DSearch(object s)
        {
            List<object[]> ol = new List<object[]>();
            foreach (object[] item in DBW.GetItemsLikeObjectArray())
            {
                if ((int)item[columnIndex] == (int)s)
                {
                    ol.Add(item);
                    return ol;
                }
            }
            return ol;
        }
        public List<object[]> OutArray(int n,char s,List<object[]> mlist)
        {
            if (mlist == null)
            {
                mlist = list;
                Clearprev();
            }
            if (nquery > n)
            {
                //return listPrev[n];
            }
            nquery = n;
            List<object[]> outList = new List<object[]>();
            char tmp;
            for (int i = 0; i < mlist.Count; i++)
            {
                if (mlist[i][columnIndex].ToString().Length > n)
                {
                    tmp = mlist[i][columnIndex].ToString().ToCharArray()[n];
                    if (tmp == s || string.Compare(tmp.ToString(), s.ToString().ToUpper()) == 0)
                    {
                        outList.Add(mlist[i]);
                    }
                }
            }
            //listPrev.Add(outList);
            return outList;
        }
        public void SearchBetween(int a,int b,bool needBorders)
        {
            List<object[]> wl =  DBW.GetItemsLikeObjectArray();
            List<object[]> ol = new List<object[]>();
            if (needBorders)
            {
                for (int i = 0; i < wl.Count; i++)
                {
                    if ((int)wl[i][columnIndex] > a && (int)wl[i][columnIndex] < b)
                        ol.Add(wl[i]);
                }
            }
            else
            {
                for (int i = 0; i < wl.Count; i++)
                {
                    if ((int)wl[i][columnIndex] >= a && (int)wl[i][columnIndex] <= b)
                        ol.Add(wl[i]);
                }
            }
            DBW.FillDGV(ol);
        }
    }
}
