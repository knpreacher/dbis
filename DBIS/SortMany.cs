﻿using System.Collections.Generic;
using System.Windows.Forms;

namespace DBIS
{
    class SortMany
    {
        int st = 0;
        int sd = 0;
        int type = 0;
        public SortMany(int st,int sd,int type)
        {
            this.st = st;
            this.sd = sd;
            this.type = type;
        }
        public SortMany(int st, int sd, int type1,int type2)
        {
            this.st = st;
            this.sd = sd;
            this.type1 = type1;
            this.type2 = type2;
        }
        List<object[]> rows;
        List<DBW.Bridge> bridges;
        private int type1;
        private int type2;

        private void GetItems()
        {
            DBW dbw = new DBW();
            bridges = dbw.GetIntems();
            bridges.Sort();
        }
        private void GetItems1()
        {
            DBW dbw = new DBW();
            rows = DBW.GetItemsLikeObjectArray();
            object t = rows[3][3];

        }
        public void Sort()
        {
            /*
             * 
             * 
             * TYPE 0
             * 
             */
            if (type1 == 0)
            {
                //if (string.Compare((string)rows[i][st], (string)rows[j][st])>0)
                GetItems1();
                object[] temp = new object[7];
                object t = null;
                //First sorting
                #region find type for first sort
                if (st == 1)//string
                {
                    for (int i = 0; i < rows.Count; i++)
                    {
                        for (int j = i + 1; j < rows.Count; j++)
                        {
                            if (string.Compare((string)rows[i][st], (string)rows[j][st]) > 0)
                            {
                                temp = rows[i];
                                rows[i] = rows[j];
                                rows[j] = temp;
                            }
                        }
                    }
                }
                else if (st == 6) //bool
                {
                    for (int i = 0; i < rows.Count; i++)
                    {
                        for (int j = i + 1; j < rows.Count; j++)
                        {
                            if ((bool)rows[i][st] && !(bool)rows[j][st])
                            {
                                temp = rows[i];
                                rows[i] = rows[j];
                                rows[j] = temp;
                            }
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < rows.Count; i++)
                    {
                        for (int j = i + 1; j < rows.Count; j++)
                        {
                            if ((int)rows[i][st] > (int)rows[j][st])
                            {
                                temp = rows[i];
                                rows[i] = rows[j];
                                rows[j] = temp;
                            }
                        }
                    }
                }
                #endregion

                if (type2 == 0)
                {
                    //second sorting
                    int k = 1;
                    int nj = 0;
                    int ej = 0;
                    List<object[]> srows = new List<object[]>();
                    while (k < rows.Count)
                    {
                        bool isf = true;
                        while (k != rows.Count && rows[k - 1][st].Equals(rows[k][st]))
                        {
                            if (isf)
                            {
                                nj = k - 1;
                                isf = false;
                            }
                            srows.Add(rows[k - 1]);
                            k++;
                        }
                        if (srows.Count > 0)
                        {
                            srows.Add(rows[k - 1]);
                            ej = k;
                            //
                            if (sd == 1)
                            {
                                for (int i = 0; i < srows.Count; i++)
                                {
                                    for (int j = i + 1; j < srows.Count; j++)
                                    {
                                        if (string.Compare((string)srows[i][sd], (string)srows[j][sd]) > 0)
                                        {
                                            temp = srows[i];
                                            srows[i] = srows[j];
                                            srows[j] = temp;
                                        }
                                    }
                                }
                            }
                            else if (sd == 6)
                            {
                                for (int i = 0; i < srows.Count; i++)
                                {
                                    for (int j = i + 1; j < srows.Count; j++)
                                    {
                                        if ((bool)srows[i][sd] && !(bool)srows[j][sd])
                                        {
                                            temp = srows[i];
                                            srows[i] = srows[j];
                                            srows[j] = temp;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                for (int i = 0; i < srows.Count; i++)
                                {
                                    for (int j = i + 1; j < srows.Count; j++)
                                    {
                                        if ((int)srows[i][sd] > (int)srows[j][sd])
                                        {
                                            temp = srows[i];
                                            srows[i] = srows[j];
                                            srows[j] = temp;
                                        }
                                    }
                                }
                            }


                            foreach (object[] item in srows)
                            {
                                rows[nj] = item;
                                nj++;
                            }
                            //

                            srows = new List<object[]>();
                        }
                        k++;
                    }

                }
                else
                {
                    int k = 1;
                    int nj = 0;
                    int ej = 0;
                    List<object[]> srows = new List<object[]>();
                    while (k < rows.Count)
                    {
                        bool isf = true;
                        while (k != rows.Count && rows[k - 1][st].Equals(rows[k][st]))
                        {
                            if (isf)
                            {
                                nj = k - 1;
                                isf = false;
                            }
                            srows.Add(rows[k - 1]);
                            k++;
                        }
                        if (srows.Count > 0)
                        {
                            srows.Add(rows[k - 1]);
                            ej = k;
                            //
                            if (sd == 1)
                            {
                                for (int i = 0; i < srows.Count; i++)
                                {
                                    for (int j = i + 1; j < srows.Count; j++)
                                    {
                                        if (string.Compare((string)srows[i][sd], (string)srows[j][sd]) < 0)
                                        {
                                            temp = srows[i];
                                            srows[i] = srows[j];
                                            srows[j] = temp;
                                        }
                                    }
                                }
                            }
                            else if (sd == 6)
                            {
                                for (int i = 0; i < srows.Count; i++)
                                {
                                    for (int j = i + 1; j < srows.Count; j++)
                                    {
                                        if (!(bool)srows[i][sd] && (bool)srows[j][sd])
                                        {
                                            temp = srows[i];
                                            srows[i] = srows[j];
                                            srows[j] = temp;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                for (int i = 0; i < srows.Count; i++)
                                {
                                    for (int j = i + 1; j < srows.Count; j++)
                                    {
                                        if ((int)srows[i][sd] < (int)srows[j][sd])
                                        {
                                            temp = srows[i];
                                            srows[i] = srows[j];
                                            srows[j] = temp;
                                        }
                                    }
                                }
                            }


                            foreach (object[] item in srows)
                            {
                                rows[nj] = item;
                                nj++;
                            }
                            //

                            srows = new List<object[]>();
                        }
                        k++;
                    }
                }
            }
            /*
             * 
             * 
             *  УБЫВАНИЕ
             * TYPE 1
             */
            else
            {
                //if (string.Compare((string)rows[i][st], (string)rows[j][st])>0)
                GetItems1();
                object[] temp = new object[7];
                object t = null;
                //First sorting
                #region find type for first sort
                if (st == 1)//string
                {
                    for (int i = 0; i < rows.Count; i++)
                    {
                        for (int j = i + 1; j < rows.Count; j++)
                        {
                            if (string.Compare((string)rows[i][st], (string)rows[j][st]) < 0)
                            {
                                temp = rows[i];
                                rows[i] = rows[j];
                                rows[j] = temp;
                            }
                        }
                    }
                }
                else if (st == 6) //bool
                {
                    for (int i = 0; i < rows.Count; i++)
                    {
                        for (int j = i + 1; j < rows.Count; j++)
                        {
                            if (!(bool)rows[i][st] && (bool)rows[j][st])
                            {
                                temp = rows[i];
                                rows[i] = rows[j];
                                rows[j] = temp;
                            }
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < rows.Count; i++)
                    {
                        for (int j = i + 1; j < rows.Count; j++)
                        {
                            if ((int)rows[i][st] < (int)rows[j][st])
                            {
                                temp = rows[i];
                                rows[i] = rows[j];
                                rows[j] = temp;
                            }
                        }
                    }
                }
                #endregion


                //second sorting
                if (type2 == 0)
                {
                    //second sorting
                    int k = 1;
                    int nj = 0;
                    int ej = 0;
                    List<object[]> srows = new List<object[]>();
                    while (k < rows.Count)
                    {
                        bool isf = true;
                        while (k != rows.Count && rows[k - 1][st].Equals(rows[k][st]))
                        {
                            if (isf)
                            {
                                nj = k - 1;
                                isf = false;
                            }
                            srows.Add(rows[k - 1]);
                            k++;
                        }
                        if (srows.Count > 0)
                        {
                            srows.Add(rows[k - 1]);
                            ej = k;
                            //
                            if (sd == 1)
                            {
                                for (int i = 0; i < srows.Count; i++)
                                {
                                    for (int j = i + 1; j < srows.Count; j++)
                                    {
                                        if (string.Compare((string)srows[i][sd], (string)srows[j][sd]) > 0)
                                        {
                                            temp = srows[i];
                                            srows[i] = srows[j];
                                            srows[j] = temp;
                                        }
                                    }
                                }
                            }
                            else if (sd == 6)
                            {
                                for (int i = 0; i < srows.Count; i++)
                                {
                                    for (int j = i + 1; j < srows.Count; j++)
                                    {
                                        if ((bool)srows[i][sd] && !(bool)srows[j][sd])
                                        {
                                            temp = srows[i];
                                            srows[i] = srows[j];
                                            srows[j] = temp;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                for (int i = 0; i < srows.Count; i++)
                                {
                                    for (int j = i + 1; j < srows.Count; j++)
                                    {
                                        if ((int)srows[i][sd] > (int)srows[j][sd])
                                        {
                                            temp = srows[i];
                                            srows[i] = srows[j];
                                            srows[j] = temp;
                                        }
                                    }
                                }
                            }


                            foreach (object[] item in srows)
                            {
                                rows[nj] = item;
                                nj++;
                            }
                            //

                            srows = new List<object[]>();
                        }
                        k++;
                    }

                }
                else
                {
                    int k = 1;
                    int nj = 0;
                    int ej = 0;
                    List<object[]> srows = new List<object[]>();
                    while (k < rows.Count)
                    {
                        bool isf = true;
                        while (k != rows.Count && rows[k - 1][st].Equals(rows[k][st]))
                        {
                            if (isf)
                            {
                                nj = k - 1;
                                isf = false;
                            }
                            srows.Add(rows[k - 1]);
                            k++;
                        }
                        if (srows.Count > 0)
                        {
                            srows.Add(rows[k - 1]);
                            ej = k;
                            //
                            if (sd == 1)
                            {
                                for (int i = 0; i < srows.Count; i++)
                                {
                                    for (int j = i + 1; j < srows.Count; j++)
                                    {
                                        if (string.Compare((string)srows[i][sd], (string)srows[j][sd]) < 0)
                                        {
                                            temp = srows[i];
                                            srows[i] = srows[j];
                                            srows[j] = temp;
                                        }
                                    }
                                }
                            }
                            else if (sd == 6)
                            {
                                for (int i = 0; i < srows.Count; i++)
                                {
                                    for (int j = i + 1; j < srows.Count; j++)
                                    {
                                        if (!(bool)srows[i][sd] && (bool)srows[j][sd])
                                        {
                                            temp = srows[i];
                                            srows[i] = srows[j];
                                            srows[j] = temp;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                for (int i = 0; i < srows.Count; i++)
                                {
                                    for (int j = i + 1; j < srows.Count; j++)
                                    {
                                        if ((int)srows[i][sd] < (int)srows[j][sd])
                                        {
                                            temp = srows[i];
                                            srows[i] = srows[j];
                                            srows[j] = temp;
                                        }
                                    }
                                }
                            }


                            foreach (object[] item in srows)
                            {
                                rows[nj] = item;
                                nj++;
                            }
                            //

                            srows = new List<object[]>();
                        }
                        k++;
                    }
                }
            }
        }

        public void Fill(DataGridView dgv)
        {
            for (int i = 0; i < rows.Count; i++)
            {
                for (int j = 0; j < 7; j++)
                {
                    dgv.Rows[i].Cells[j].Value = rows[i][j];
                }
            }
        }
    }
}
