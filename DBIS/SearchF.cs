﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace DBIS
{
    public partial class SearchF : Form
    {
        private RadioButton[] rbs;
        int st = -1;
        int sortType = -1;
        public SearchF(int st,int type)
        {
            InitializeComponent();
            this.st = st;
            sortType = type;
            rbs = new RadioButton[] {
                radioButton1,
                radioButton2,
                radioButton3,
                radioButton4,
                radioButton5,
                radioButton6,
                radioButton7
            };
        }
        Search search;
        private void SearchF_Load(object sender, EventArgs e)
        {
            bool isf = true;
            search = new Search(st);
            search.SetRBCheckedArray(
                    rbs[0].Checked,
                    rbs[1].Checked,
                    rbs[2].Checked,
                    rbs[3].Checked,
                    rbs[4].Checked,
                    rbs[5].Checked,
                    rbs[6].Checked
                );
            search.CheckSortedState();
            for (int i = 0; i < rbs.Length; i++)
            {
                rbs[i].CheckedChanged += new EventHandler((object sender1, EventArgs e1) => {
                    if (isf)
                    {
                        search = new Search(st);
                        search.SetRBCheckedArray(
                                rbs[0].Checked,
                                rbs[1].Checked,
                                rbs[2].Checked,
                                rbs[3].Checked,
                                rbs[4].Checked,
                                rbs[5].Checked,
                                rbs[6].Checked
                            );

                        search.CheckSortedState();
                        isf = false;
                        return;
                    }
                });
            }
            isf = true;
        }
        List<object[]> ll;
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (!search.conts)
            {
                listBox1.Items.Clear();
                if (textBox1.TextLength != 0)
                {

                    if (textBox1.TextLength == 1)
                    {
                        ll = null;
                    }

                    //ll = null;
                    ll = search.OutArray(textBox1.TextLength - 1, textBox1.Text.ToCharArray()[textBox1.TextLength - 1], ll);
                    listBox1.Visible = true;
                    int i = 0;
                    foreach (object[] item in ll)
                    {
                        listBox1.Items.Add((string)item[1]);
                        //MainF.sdgv.Rows[i].Cells[0].Value = item[1];
                        i++;
                    }
                    DBW.FillDGV(ll);
                }
                else
                {
                    DBW.FillDGV(DBW.GetItemsLikeObjectArray());
                    listBox1.Visible = false;
                }
            }
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            radioButton2.Enabled = tabControl1.SelectedIndex == 0 ? true : false;
            radioButton7.Enabled = radioButton2.Enabled;
            radioButton1.Checked = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (tabControl1.SelectedIndex == 1)
            {
                int a, b;
                /*
                if (textBox2.Text == "") a = -1;
                else if (textBox3.Text == "") b = 999999999;
                */
                if(int.TryParse(textBox2.Text,out a) && int.TryParse(textBox3.Text, out b))
                {
                    search.SearchBetween(a, b, cbNB.Checked);
                }
                else
                {
                    MessageBox.Show("Неверные данные", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                DBW.FillDGV(search.DSearch(int.Parse(textBox1.Text)));
            }
        }

        private void SearchF_FormClosed(object sender, FormClosedEventArgs e)
        {
            MainF.lsc = -1;
        }
    }
}
